
public class Jogador {
	private int pontos;

	public Jogador() {
		this.pontos = 0;
	}
	private void acumularPontos(Carta carta) {
		pontos += carta.getValor();
	}
	private String mostrarMao(Carta carta) {
		String mensagem;
		mensagem = "Sua carta é " + carta.getNomeRank() + " de " + carta.getNaipe() + " e vale " + carta.getValor() + 
				" ponto(s) \n => Você está com " + this.pontos + " pontos" ;
		return mensagem;
	}
	public String receberCarta(Carta carta) {
		this.acumularPontos(carta);
		return this.mostrarMao(carta);
	}
	public int getPontos() {
		return this.pontos;
	}
}
