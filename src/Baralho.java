import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Baralho {
	private int[] numeros = {1,2,3,4,5,6,7,8,9,10,11,12,13};
	private String[] naipes = {"copas", "ouros", "espadas", "paus"};
	private List<Carta> cartas = new ArrayList<Carta>();
	private List<Integer> ordemCartas = new ArrayList<Integer>();
	
	public Baralho() {
		for (int numero : numeros) {
			for (String naipe : naipes) {
				this.cartas.add(new Carta(numero, naipe));
			}
		}
		this.embaralhar();
	}
	
	public void imprimirCartas(boolean ordem) {
		if(ordem) {
			for (Carta carta : cartas) {
				System.out.println("Carta: " + carta.getRank() + " Naipe: " + carta.getNaipe());
			}
		}
		else {
			for (Integer indice: ordemCartas) {
				System.out.println("Carta: " + cartas.get(indice).getRank() + " Naipe: " + cartas.get(indice).getNaipe());
			}
		}
	}
	
	public void embaralhar() {
		Random random = new Random();
		int sorteado, tamanhoBaralho;
		
		tamanhoBaralho = this.cartas.size();
		for (int i = 0; i < tamanhoBaralho; i++) {
			sorteado = random.nextInt(this.cartas.size());
			while(this.ordemCartas.contains(sorteado)) {
				sorteado = random.nextInt(tamanhoBaralho);
			}
			this.ordemCartas.add(sorteado);
		}
	}
	public Carta getCarta() {
		int carta;
		if (!this.ordemCartas.isEmpty()) {
			carta = this.ordemCartas.remove(0);
			return this.cartas.get(carta);
		}
		return null;
	}
}
	