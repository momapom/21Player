
public class Casino {
	public static void main(String[] args) {
		Crupie crupie = new Crupie();
		Jogador jogador = new Jogador();
		Carta carta;
		String mensagem;
		int pontos;
		
		System.out.println("Bem vindo ao Cassino Mastertech \n");

		do {
			carta = crupie.novaCarta();
	   		mensagem = jogador.receberCarta(carta);
	   		pontos = jogador.getPontos();
	   		
	   		System.out.println(mensagem);
		}while (crupie.validarRodada(pontos));
		
	}
}
