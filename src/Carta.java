
public class Carta {
	private int rank;
	private String naipe;
	private int valorEfetivo;
	private String nomeRank;
	
	public Carta(int numero, String naipe) {
		this.rank = numero;
		this.naipe = naipe;
		
		if(numero >= 10)
			this.valorEfetivo = 10;
		else
			this.valorEfetivo = numero;
		
		if (numero == 1)
			this.nomeRank = "As";
		else
			if (numero == 11)
				this.nomeRank = "Valete";
			else
				if (numero == 12)
					this.nomeRank = "Dama";
				else
					if (numero == 13)
						this.nomeRank = "Rei";
					else
						this.nomeRank = Integer.toString(numero);
				
	}
	
	public int getRank() {
		return this.rank;
	}
	
	public String getNaipe() {
		return this.naipe;
	}
	
	public int getValor() {
		return this.valorEfetivo;
	}
	
	public String getNomeRank() {
		return this.nomeRank;
	}
}
